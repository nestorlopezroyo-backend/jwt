package com.techuniversity.jwt.components;

import org.jose4j.jwa.AlgorithmConstraints;
import org.jose4j.jwk.RsaJsonWebKey;
import org.jose4j.jwk.RsaJwkGenerator;
import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.MalformedClaimException;
import org.jose4j.jwt.consumer.ErrorCodes;
import org.jose4j.jwt.consumer.InvalidJwtException;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;
import org.jose4j.lang.JoseException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.security.auth.message.AuthException;

@Component
public class JWTBuilder {

    @Value("${jwt.issuer}")
    private String jwtIssuer;

    @Value("${jwt.secret}")
    private String jwtSecret;

    @Value("${jwt.expiry}")
    private float jwtExpiry;

    RsaJsonWebKey rsaJsonWebKey;

    public JWTBuilder() {
    }

    public String generateToken(String userId, String roles) {
        /*Aqui deberia autentificarse contra una base de datos o otro servicio*/
        try {
            /*Configuro*/
            JwtClaims jwtClaims = new JwtClaims();//Es la afirmación de lo que soy.
            jwtClaims.setIssuer(jwtIssuer); //Identificador del quien genera los tokens.
            jwtClaims.setExpirationTimeMinutesInTheFuture(jwtExpiry); // Tiempo de expiración del token
            jwtClaims.setAudience("ALL"); //Podran invocarlos cualquiera
            jwtClaims.setStringListClaim("groups", roles);
            jwtClaims.setGeneratedJwtId();
            jwtClaims.setIssuedAtToNow();//Se guarda el tiempo de creación
            jwtClaims.setSubject("AUTHTOKEN");
            jwtClaims.setClaim("userId", userId); //Indicamos el userId.

            JsonWebSignature jws = new JsonWebSignature(); // Utilidad de firma
            jws.setPayload(jwtClaims.toJson()); // convierte todo el claim en json y se lo pasa como payload.
            jws.setKey(rsaJsonWebKey.getRsaPrivateKey());//
            jws.setKeyIdHeaderValue(rsaJsonWebKey.getKeyId()); // Clave publica por header
            jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.RSA_USING_SHA256); //Algoritmo a usar


            return jws.getCompactSerialization(); // El token

        } catch (JoseException josex) {
            josex.printStackTrace();
            return null;
        }
    }

    /* Obtenemos el token y lo deshago parseandolo*/

    public JwtClaims generateParseToken(String token) throws Exception {
        JwtConsumer jwtConsumer = new JwtConsumerBuilder()
                .setRequireExpirationTime()
                .setSkipSignatureVerification()
                .setAllowedClockSkewInSeconds(60) // Valida expiración cada 60 segs.
                .setRequireSubject() //Requerido Sujeto
                .setExpectedSubject("AUTHTOKEN") //Sujeto Esperado
                .setExpectedIssuer(jwtIssuer)
                .setExpectedAudience("ALL") //Audiencia esperada
                .setVerificationKey(rsaJsonWebKey.getKey())
                .setJwsAlgorithmConstraints(new AlgorithmConstraints(
                        AlgorithmConstraints.ConstraintType.WHITELIST,
                        AlgorithmIdentifiers.RSA_USING_SHA256
                )) // Algoritmos con cual parsear y obtener la afirmación(claim)
                .build();

        try {
            JwtClaims jwtClaims = jwtConsumer.processToClaims(token); // Con el consumirdor y el token que claims obtienes.
            return jwtClaims;
        } catch (InvalidJwtException e) {
            try{
            if (e.hasExpired()) {
                throw new Exception(String.format("Caducada en: %s", e.getJwtContext().getJwtClaims().getExpirationTime()));
            }

            if(e.hasErrorCode(ErrorCodes.AUDIENCE_INVALID)){
                throw new Exception(String.format("Audiencia incorrecta: %s", e.getJwtContext().getJwtClaims().getAudience()));
            }

            throw  new AuthException(e.getMessage());
            }
            catch (MalformedClaimException innerEx){
                throw new AuthException("Mal formada");
            }
        }
    }

    public String validarToken(String token) throws Exception{

        String userId = null;
        JwtClaims jwtClaims = generateParseToken(token);
        userId = jwtClaims.getClaimValue("userId").toString();
        return userId;

    }
    /*
     * Se genera despues de la construcción
     */
    @PostConstruct
    public void init() {
        try {
            rsaJsonWebKey = RsaJwkGenerator.generateJwk(2048); // Generador de claves
            rsaJsonWebKey.setKeyId(jwtSecret);

        } catch (JoseException josex) {
            josex.printStackTrace();
        }
    }
}
