package com.techuniversity.jwt.controllers;

import com.techuniversity.jwt.components.JWTBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("techu/jwt")
public class HelloWorldController {

    @Autowired
    JWTBuilder jwtBuilder;

    @GetMapping("/tokenget")
    public String tokenget(@RequestParam(value = "nombre", defaultValue = "Techu") String name) {
        //Deberia recibir user y password (este no por params)
        return jwtBuilder.generateToken(name, "admin");
    }

    @GetMapping(path = "/hola", headers = {"Authorization"}) //Requerimos el headers "Authorizaction"
    public String hola(@RequestHeader("Authorization") String token) {
        /* Logica de autorizaciones*/
        String userId;
        try {
            userId = jwtBuilder.validarToken(token);
        } catch (Exception ex) {
            return ex.getMessage();
        }

        /* Logica de negocio*/

        String s = String.format("Saludos para %s desde Demo JWT", userId);
        return s;

    }
}
